﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Controller : MonoBehaviour {

	public GameReference gameReference;
	public GameSettings gameSettings;
	public AudioSource music;

	private float gameOverTime;
	private float gameStartTime;

	public static GameState state = GameState.PLAY;

	private List<GameObject> presents = new List<GameObject>();


	public enum GameState {
		PLAY,
		OUTRO,
		WAITING
	}

	private float spawnTime;

	void Start () {
		music.Play();
		gameOverTime = Time.time + gameSettings.gameoverTime;
		gameStartTime = Time.time + gameSettings.gameStartTime;

	}
	
	void Update () {

		switch(state) {
			case GameState.PLAY:
				PlayState();
				break;
			case GameState.OUTRO:
				OutroState();
				break;
			case GameState.WAITING:
				if (Input.GetKeyDown(KeyCode.Space)) {
					gameReference.sledgeReference.transform.position = new Vector3(5, 2.2f, 0);
					state = GameState.PLAY;
					gameReference.uiReferences.scoreReference.score = 0;
					gameReference.uiReferences.scoreReference.gameObject.SetActive(true);
					gameReference.uiReferences.speechReference.text.text = "";
					gameReference.uiReferences.pressSpace.text = "";

					Start();
				}
				break;
		}

	}

	private void PlayState() {
		if(Time.time < gameStartTime) {
			return;
		}

		if (Time.time >= spawnTime) {
			SpawnPresent();
		}

		if(Time.time >= gameOverTime) {
			state = GameState.OUTRO;
		}
	}

	private void OutroState() {

		SledgeReference sledge = gameReference.sledgeReference;
		UIReference ui = gameReference.uiReferences;

		sledge.animator.enabled = false;
		ui.speechReference.text.text = "Merry Christmas";
		ui.pressSpace.text = "You Got "+ui.scoreReference.score+" Packages\n\nPress Space To Play Again";

		sledge.transform.DOLocalMoveX(10, 0.1f);
		ui.scoreReference.gameObject.SetActive(false);

		foreach (GameObject go in presents) {
			Destroy(go);
		}

		presents.Clear();

		state = GameState.WAITING;
	}

	private void SpawnPresent() {
		PresentSpawner spawner = gameSettings.presentSpawner;

		spawnTime = Time.time + spawner.spawnCurve.Evaluate(Time.time);

		PresentReference present = spawner.Spawn(gameReference.presentContainer.transform);

		presents.Add(present.gameObject);

		present.collected += ChangeScore;

		float x = Random.Range(gameSettings.presentSpawner.minX, gameSettings.presentSpawner.maxX);
		float y = Random.Range(gameSettings.presentSpawner.minY, gameSettings.presentSpawner.maxY);

		present.shadowReference.enabled = true;

		present.shadowReference.endY = y;
		gameReference.playerReference.audioSource.clip = gameSettings.throwing[Random.Range(0, 2)];

		present.transform.DOJump(
			new Vector3(x, y, 0f),
			0.8f,
			1,
			0.2f).onComplete = () => {
				present.GetComponent<MoveOutOfTheScreen>().enabled = false;
				present.isCollectable = true;
				present.audioSource.clip = gameSettings.hit[Random.Range(0, 2)];
				present.audioSource.Play();
		};
	}

	private void ChangeScore(int value) {
		gameReference.uiReferences.scoreReference.score += value;
	}
}
