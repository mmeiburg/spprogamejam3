﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIReference : MonoBehaviour {
	public SpeechReference speechReference;
	public ScoreReference scoreReference;
	public Text pressSpace;
}
