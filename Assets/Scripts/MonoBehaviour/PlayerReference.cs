using System;
using UnityEngine;
using DG.Tweening;

public class PlayerReference : MonoBehaviour {

	public PlayerSettings settings;

	public Animator animator;
	public SpriteRenderer spriteRenderer;
	public Rigidbody2D rb2d;
	public AudioSource audioSource;

	public void Update() {

		bool isRunning = Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0;

		animator.SetBool("isRunning", isRunning);

		if(transform.position.x < -10) {
			transform.position = new Vector3(0f, 5.9f, 0f);
			transform.DOJump(new Vector3(0f, -1.9f, 0f), 4, 1, 0.5f);
		}

	}

	public void Throw() {
		animator.SetTrigger("throw");
	}

	private void FixedUpdate() {

		float dx = 0;
		float dy = 0;

		if (Input.GetKey(KeyCode.W)) {
			dy = 1;
		} else if (Input.GetKey(KeyCode.S)) {
			dy = -1;
		}

		if (Input.GetKey(KeyCode.A)) {
			dx = -1;
			spriteRenderer.flipX = true;
		} else if (Input.GetKey(KeyCode.D)) {
			dx = 1;
			spriteRenderer.flipX = false;
		}


		if (dx == 0 && dy == 0) {
			animator.ResetTrigger("throw");

			if (Controller.GameState.PLAY != Controller.state) {
				rb2d.velocity = Vector2.zero;
				//rb2d.velocity = new Vector2(-1, dy) * settings.speed / 1.65f;
			}

			//rb2d.velocity = Vector2.zero;

		} else {


			if ((transform.position.y >= -0.6f && dy == 1) || (transform.position.y <= -4f && dy == -1)) {
				dy = 0;
			}

			if ((transform.position.x >= 7f && dx == 1) || (transform.position.x <= -7f && dx == -1)) {
				dx = 0;
			}

			float speed = settings.speed;

			if (dx == -1 && Controller.GameState.PLAY == Controller.state) {
				speed = 6f;
			}


			rb2d.velocity = new Vector2(dx, dy) * speed;


		}
	}
}