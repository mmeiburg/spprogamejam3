﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOutOfTheScreen : MonoBehaviour {

	public Rigidbody2D body;
	public float speed = 3f;

	void Update () {

		if(Controller.GameState.PLAY != Controller.state) {
			return;
		}

		body.velocity = (Vector2.left * speed);

	}
}
