﻿using UnityEngine;

public class GameReference : MonoBehaviour {

	public UIReference uiReferences;

	public PlayerReference playerReference;
	public SledgeReference sledgeReference;
	public GameObject presentContainer;
}

