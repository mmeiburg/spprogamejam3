﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreReference : MonoBehaviour {
	public Text text;

	public int score;

	private void Update() {
		text.text = "" + score;
	}
}
