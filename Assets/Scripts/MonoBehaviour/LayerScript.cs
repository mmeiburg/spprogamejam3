﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerScript : MonoBehaviour {

	private SpriteRenderer spriteRenderer;
	public bool isStatic = true;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.sortingOrder = -(int)transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(!isStatic)
			spriteRenderer.sortingOrder = -(int)transform.position.y;
	}
}
