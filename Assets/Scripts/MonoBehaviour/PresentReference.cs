﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PresentReference : MonoBehaviour {

	public int score;
	public Action<int> collected;

	public bool isCollectable;

	public ShadowReference shadowReference;
	public SpriteRenderer spriteRenderer;
	public AudioSource audioSource;

	private void OnTriggerEnter2D(Collider2D collision) {
		if(!isCollectable) {
			return;
		}

		if (collision.gameObject.tag == "Player") {
			collision.GetComponent<PlayerReference>().audioSource.Play();
			GetComponent<MoveOutOfTheScreen>().enabled = true;
			collision.gameObject.SendMessage("Throw");

			float sledgeX = UnityEngine.Random.Range(5 - 0.5f, 5 + 0.5f);
			float sledgeY = 2f;

			shadowReference.endY = sledgeY;

			transform.DOJump(new Vector2(3.5f, 8), 1f, 1, 1f).onComplete = () => {
				shadowReference.gameObject.SetActive(false);
				ChangeScore(score);

				GetComponent<BoxCollider2D>().isTrigger = false;
				transform.DOJump(new Vector2(sledgeX, sledgeY), 2f, 1, 0.5f).onComplete = () => {
					Destroy(gameObject);
				};

			};
		}
	}

	public void Update() {

		MoveShadow();

		if (transform.position.x < -8) {
			Destroy(gameObject);
			ChangeScore(-score);
		}
	}

	private void MoveShadow() {
		//float y = Mathf.Lerp(shadowReference.transform.position.y, shadowReference.endY, Time.deltaTime); ;

		/*float m = shadowReference.endY / 5;
		float q = shadowReference.endY;



		float y = m * shadowReference.transform.position.x + q;*/

		shadowReference.transform.position = new Vector3(transform.position.x, shadowReference.endY, 0f);

	}

	private void ChangeScore(int value) {
		if (collected != null) {
			collected.Invoke(value);
		}
	}
}
