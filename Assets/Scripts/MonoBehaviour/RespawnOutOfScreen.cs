﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnOutOfScreen : MonoBehaviour {
	void Update () {
		if(transform.position.x <= -20) {
			float x = 20;
			transform.position = new Vector3(x, transform.position.y, 0f);
		}
	}
}
