﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/PresentSpawner")]
public class PresentSpawner : ScriptableObject {
	public AnimationCurve spawnCurve;
	public GameObject presentPrefab;
	public Sprite[] sprites;

	public float spawnX;
	public float spawnY;

	public float minY;
	public float maxY;

	public float minX;
	public float maxX;

	public PresentReference Spawn(Transform parent) {
		//var position = Random.insideUnitCircle.normalized * settings.distanceSpawnPointAstroid;

		GameObject go = Object.Instantiate(presentPrefab, new Vector3(spawnX, spawnY, 0f), new Quaternion(), parent);

		//go.transform.position = new Vector3(5f, 3f, 0f);

		PresentReference reference = go.GetComponent<PresentReference>();

		if(sprites.Length > 0)
			reference.spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];

		return reference;
	}
}

