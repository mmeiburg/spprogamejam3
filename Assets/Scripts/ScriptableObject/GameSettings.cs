﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/GameSettings")]
public class GameSettings : ScriptableObject {
	public PlayerSettings playerSettings;
	public PresentSpawner presentSpawner;

	public float gameoverTime;
	public float gameStartTime;

	public AudioClip[] hit;
	public AudioClip song;
	public AudioClip[] throwing;
}
